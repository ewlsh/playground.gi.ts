Based on [`gi.ts`](https://gitlab.gnome.org/ewlsh/gi.ts).

How to reproduce:
1. `yarn link @gi.ts/cli`
2. `gi-ts config --lock`
3. `gi-ts generate`

How to build and test the definitions:

```sh
tsc
```