/**
 * GstTranscoder 1.0
 *
 * Generated from 1.0
 */

import * as GObject from "gobject";
import * as Gst from "gst";
import * as GstPbutils from "gstpbutils";
import * as GLib from "glib";

export function transcoder_error_get_name(error: TranscoderError): string;
export function transcoder_error_quark(): GLib.Quark;

export class TranscoderError extends GLib.Error {
    static $gtype: GObject.GType<TranscoderError>;

    constructor(options: { message: string; code: number });
    constructor(copy: TranscoderError);

    // Properties
    static FAILED: number;

    // Members
    static get_name(error: TranscoderError): string;
    static quark(): GLib.Quark;
}
export module Transcoder {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
        avoid_reencoding: boolean;
        avoidReencoding: boolean;
        dest_uri: string;
        destUri: string;
        duration: number;
        pipeline: Gst.Element;
        position: number;
        position_update_interval: number;
        positionUpdateInterval: number;
        profile: GstPbutils.EncodingProfile;
        signal_dispatcher: TranscoderSignalDispatcher;
        signalDispatcher: TranscoderSignalDispatcher;
        src_uri: string;
        srcUri: string;
    }
}
export class Transcoder extends Gst.Object {
    static $gtype: GObject.GType<Transcoder>;

    constructor(properties?: Partial<Transcoder.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<Transcoder.ConstructorProperties>, ...args: any[]): void;

    // Properties
    avoid_reencoding: boolean;
    avoidReencoding: boolean;
    dest_uri: string;
    destUri: string;
    duration: number;
    pipeline: Gst.Element;
    position: number;
    position_update_interval: number;
    positionUpdateInterval: number;
    profile: GstPbutils.EncodingProfile;
    signal_dispatcher: TranscoderSignalDispatcher;
    signalDispatcher: TranscoderSignalDispatcher;
    src_uri: string;
    srcUri: string;

    // Signals

    connect(id: string, callback: (...args: any[]) => any): number;
    connect_after(id: string, callback: (...args: any[]) => any): number;
    emit(id: string, ...args: any[]): void;
    connect(signal: "done", callback: (_source: this) => void): number;
    connect_after(signal: "done", callback: (_source: this) => void): number;
    emit(signal: "done"): void;
    connect(signal: "duration-changed", callback: (_source: this, object: number) => void): number;
    connect_after(signal: "duration-changed", callback: (_source: this, object: number) => void): number;
    emit(signal: "duration-changed", object: number): void;
    connect(signal: "error", callback: (_source: this, object: GLib.Error, p0: Gst.Structure) => void): number;
    connect_after(signal: "error", callback: (_source: this, object: GLib.Error, p0: Gst.Structure) => void): number;
    emit(signal: "error", object: GLib.Error, p0: Gst.Structure): void;
    connect(signal: "position-updated", callback: (_source: this, object: number) => void): number;
    connect_after(signal: "position-updated", callback: (_source: this, object: number) => void): number;
    emit(signal: "position-updated", object: number): void;
    connect(signal: "warning", callback: (_source: this, object: GLib.Error, p0: Gst.Structure) => void): number;
    connect_after(signal: "warning", callback: (_source: this, object: GLib.Error, p0: Gst.Structure) => void): number;
    emit(signal: "warning", object: GLib.Error, p0: Gst.Structure): void;

    // Constructors

    static ["new"](source_uri: string, dest_uri: string, encoding_profile: string): Transcoder;
    static new_full(
        source_uri: string,
        dest_uri: string,
        profile: GstPbutils.EncodingProfile,
        signal_dispatcher: TranscoderSignalDispatcher
    ): Transcoder;

    // Members

    get_avoid_reencoding(): boolean;
    get_dest_uri(): string;
    get_duration(): Gst.ClockTime;
    get_pipeline(): Gst.Element;
    get_position(): Gst.ClockTime;
    get_position_update_interval(): number;
    get_source_uri(): string;
    run(): boolean;
    run_async(): void;
    set_avoid_reencoding(avoid_reencoding: boolean): void;
    set_cpu_usage(cpu_usage: number): void;
    set_position_update_interval(interval: number): void;
}
export module TranscoderGMainContextSignalDispatcher {
    export interface ConstructorProperties extends GObject.Object.ConstructorProperties {
        [key: string]: any;
        application_context: GLib.MainContext;
        applicationContext: GLib.MainContext;
    }
}
export class TranscoderGMainContextSignalDispatcher extends GObject.Object implements TranscoderSignalDispatcher {
    static $gtype: GObject.GType<TranscoderGMainContextSignalDispatcher>;

    constructor(properties?: Partial<TranscoderGMainContextSignalDispatcher.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<TranscoderGMainContextSignalDispatcher.ConstructorProperties>, ...args: any[]): void;

    // Properties
    application_context: GLib.MainContext;
    applicationContext: GLib.MainContext;

    // Members

    static new(application_context?: GLib.MainContext | null): TranscoderSignalDispatcher;

    // Implemented Members

    vfunc_dispatch(transcoder: Transcoder, emitter?: any | null, data?: any | null): void;
}

export class TranscoderPrivate {
    static $gtype: GObject.GType<TranscoderPrivate>;

    constructor(copy: TranscoderPrivate);
}

export interface TranscoderSignalDispatcherNamespace {
    $gtype: GObject.GType<TranscoderSignalDispatcher>;
    prototype: TranscoderSignalDispatcherPrototype;
}
export type TranscoderSignalDispatcher = TranscoderSignalDispatcherPrototype;
export interface TranscoderSignalDispatcherPrototype extends GObject.Object {
    // Members

    vfunc_dispatch(transcoder: Transcoder, emitter?: any | null, data?: any | null): void;
}

export const TranscoderSignalDispatcher: TranscoderSignalDispatcherNamespace;
