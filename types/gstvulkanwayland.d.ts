/**
 * GstVulkanWayland 1.0
 *
 * Generated from 1.0
 */

import * as Gst from "gst";
import * as GstBase from "gstbase";
import * as GstVideo from "gstvideo";
import * as GstVulkan from "gstvulkan";
import * as Vulkan from "vulkan";
import * as GObject from "gobject";

export module VulkanDisplayWayland {
    export interface ConstructorProperties extends GstVulkan.VulkanDisplay.ConstructorProperties {
        [key: string]: any;
    }
}
export class VulkanDisplayWayland extends GstVulkan.VulkanDisplay {
    static $gtype: GObject.GType<VulkanDisplayWayland>;

    constructor(properties?: Partial<VulkanDisplayWayland.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<VulkanDisplayWayland.ConstructorProperties>, ...args: any[]): void;

    // Fields
    display: any;
    registry: any;
    compositor: any;
    subcompositor: any;
    shell: any;

    // Constructors

    static ["new"](name?: string | null): VulkanDisplayWayland;
    static ["new"](...args: never[]): never;
    static new_with_display(display?: any | null): VulkanDisplayWayland;
}
