/**
 * GstWebRTC 1.0
 *
 * Generated from 1.0
 */

import * as Gst from "gst";
import * as GstSdp from "gstsdp";
import * as GObject from "gobject";
import * as GLib from "glib";

export function webrtc_sdp_type_to_string(type: WebRTCSDPType): string;

export namespace WebRTCBundlePolicy {
    export const $gtype: GObject.GType<WebRTCBundlePolicy>;
}

export enum WebRTCBundlePolicy {
    NONE = 0,
    BALANCED = 1,
    MAX_COMPAT = 2,
    MAX_BUNDLE = 3,
}

export namespace WebRTCDTLSSetup {
    export const $gtype: GObject.GType<WebRTCDTLSSetup>;
}

export enum WebRTCDTLSSetup {
    NONE = 0,
    ACTPASS = 1,
    ACTIVE = 2,
    PASSIVE = 3,
}

export namespace WebRTCDTLSTransportState {
    export const $gtype: GObject.GType<WebRTCDTLSTransportState>;
}

export enum WebRTCDTLSTransportState {
    NEW = 0,
    CLOSED = 1,
    FAILED = 2,
    CONNECTING = 3,
    CONNECTED = 4,
}

export namespace WebRTCDataChannelState {
    export const $gtype: GObject.GType<WebRTCDataChannelState>;
}

export enum WebRTCDataChannelState {
    NEW = 0,
    CONNECTING = 1,
    OPEN = 2,
    CLOSING = 3,
    CLOSED = 4,
}

export namespace WebRTCFECType {
    export const $gtype: GObject.GType<WebRTCFECType>;
}

export enum WebRTCFECType {
    NONE = 0,
    ULP_RED = 1,
}

export namespace WebRTCICEComponent {
    export const $gtype: GObject.GType<WebRTCICEComponent>;
}

export enum WebRTCICEComponent {
    RTP = 0,
    RTCP = 1,
}

export namespace WebRTCICEConnectionState {
    export const $gtype: GObject.GType<WebRTCICEConnectionState>;
}

export enum WebRTCICEConnectionState {
    NEW = 0,
    CHECKING = 1,
    CONNECTED = 2,
    COMPLETED = 3,
    FAILED = 4,
    DISCONNECTED = 5,
    CLOSED = 6,
}

export namespace WebRTCICEGatheringState {
    export const $gtype: GObject.GType<WebRTCICEGatheringState>;
}

export enum WebRTCICEGatheringState {
    NEW = 0,
    GATHERING = 1,
    COMPLETE = 2,
}

export namespace WebRTCICERole {
    export const $gtype: GObject.GType<WebRTCICERole>;
}

export enum WebRTCICERole {
    CONTROLLED = 0,
    CONTROLLING = 1,
}

export namespace WebRTCICETransportPolicy {
    export const $gtype: GObject.GType<WebRTCICETransportPolicy>;
}

export enum WebRTCICETransportPolicy {
    ALL = 0,
    RELAY = 1,
}

export namespace WebRTCPeerConnectionState {
    export const $gtype: GObject.GType<WebRTCPeerConnectionState>;
}

export enum WebRTCPeerConnectionState {
    NEW = 0,
    CONNECTING = 1,
    CONNECTED = 2,
    DISCONNECTED = 3,
    FAILED = 4,
    CLOSED = 5,
}

export namespace WebRTCPriorityType {
    export const $gtype: GObject.GType<WebRTCPriorityType>;
}

export enum WebRTCPriorityType {
    VERY_LOW = 1,
    LOW = 2,
    MEDIUM = 3,
    HIGH = 4,
}

export namespace WebRTCRTPTransceiverDirection {
    export const $gtype: GObject.GType<WebRTCRTPTransceiverDirection>;
}

export enum WebRTCRTPTransceiverDirection {
    NONE = 0,
    INACTIVE = 1,
    SENDONLY = 2,
    RECVONLY = 3,
    SENDRECV = 4,
}

export namespace WebRTCSCTPTransportState {
    export const $gtype: GObject.GType<WebRTCSCTPTransportState>;
}

export enum WebRTCSCTPTransportState {
    NEW = 0,
    CONNECTING = 1,
    CONNECTED = 2,
    CLOSED = 3,
}

export namespace WebRTCSDPType {
    export const $gtype: GObject.GType<WebRTCSDPType>;
}

export enum WebRTCSDPType {
    OFFER = 1,
    PRANSWER = 2,
    ANSWER = 3,
    ROLLBACK = 4,
}

export namespace WebRTCSignalingState {
    export const $gtype: GObject.GType<WebRTCSignalingState>;
}

export enum WebRTCSignalingState {
    STABLE = 0,
    CLOSED = 1,
    HAVE_LOCAL_OFFER = 2,
    HAVE_REMOTE_OFFER = 3,
    HAVE_LOCAL_PRANSWER = 4,
    HAVE_REMOTE_PRANSWER = 5,
}

export namespace WebRTCStatsType {
    export const $gtype: GObject.GType<WebRTCStatsType>;
}

export enum WebRTCStatsType {
    CODEC = 1,
    INBOUND_RTP = 2,
    OUTBOUND_RTP = 3,
    REMOTE_INBOUND_RTP = 4,
    REMOTE_OUTBOUND_RTP = 5,
    CSRC = 6,
    PEER_CONNECTION = 7,
    DATA_CHANNEL = 8,
    STREAM = 9,
    TRANSPORT = 10,
    CANDIDATE_PAIR = 11,
    LOCAL_CANDIDATE = 12,
    REMOTE_CANDIDATE = 13,
    CERTIFICATE = 14,
}
export module WebRTCDTLSTransport {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
        certificate: string;
        client: boolean;
        remote_certificate: string;
        remoteCertificate: string;
        rtcp: boolean;
        session_id: number;
        sessionId: number;
        state: WebRTCDTLSTransportState;
        transport: WebRTCICETransport;
    }
}
export class WebRTCDTLSTransport extends Gst.Object {
    static $gtype: GObject.GType<WebRTCDTLSTransport>;

    constructor(properties?: Partial<WebRTCDTLSTransport.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCDTLSTransport.ConstructorProperties>, ...args: any[]): void;

    // Properties
    certificate: string;
    client: boolean;
    remote_certificate: string;
    remoteCertificate: string;
    rtcp: boolean;
    session_id: number;
    sessionId: number;
    state: WebRTCDTLSTransportState;
    transport: WebRTCICETransport;

    // Fields
    is_rtcp: boolean;
    dtlssrtpenc: Gst.Element;
    dtlssrtpdec: Gst.Element;

    // Constructors

    static ["new"](session_id: number, rtcp: boolean): WebRTCDTLSTransport;

    // Members

    set_transport(ice: WebRTCICETransport): void;
}
export module WebRTCDataChannel {
    export interface ConstructorProperties extends GObject.Object.ConstructorProperties {
        [key: string]: any;
        buffered_amount: number;
        bufferedAmount: number;
        buffered_amount_low_threshold: number;
        bufferedAmountLowThreshold: number;
        id: number;
        label: string;
        max_packet_lifetime: number;
        maxPacketLifetime: number;
        max_retransmits: number;
        maxRetransmits: number;
        negotiated: boolean;
        ordered: boolean;
        priority: WebRTCPriorityType;
        protocol: string;
        ready_state: WebRTCDataChannelState;
        readyState: WebRTCDataChannelState;
    }
}
export abstract class WebRTCDataChannel extends GObject.Object {
    static $gtype: GObject.GType<WebRTCDataChannel>;

    constructor(properties?: Partial<WebRTCDataChannel.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCDataChannel.ConstructorProperties>, ...args: any[]): void;

    // Properties
    buffered_amount: number;
    bufferedAmount: number;
    buffered_amount_low_threshold: number;
    bufferedAmountLowThreshold: number;
    id: number;
    label: string;
    max_packet_lifetime: number;
    maxPacketLifetime: number;
    max_retransmits: number;
    maxRetransmits: number;
    negotiated: boolean;
    ordered: boolean;
    priority: WebRTCPriorityType;
    protocol: string;
    ready_state: WebRTCDataChannelState;
    readyState: WebRTCDataChannelState;

    // Fields
    lock: GLib.Mutex;

    // Signals

    connect(id: string, callback: (...args: any[]) => any): number;
    connect_after(id: string, callback: (...args: any[]) => any): number;
    emit(id: string, ...args: any[]): void;
    connect(signal: "close", callback: (_source: this) => void): number;
    connect_after(signal: "close", callback: (_source: this) => void): number;
    emit(signal: "close"): void;
    connect(signal: "on-buffered-amount-low", callback: (_source: this) => void): number;
    connect_after(signal: "on-buffered-amount-low", callback: (_source: this) => void): number;
    emit(signal: "on-buffered-amount-low"): void;
    connect(signal: "on-close", callback: (_source: this) => void): number;
    connect_after(signal: "on-close", callback: (_source: this) => void): number;
    emit(signal: "on-close"): void;
    connect(signal: "on-error", callback: (_source: this, error: GLib.Error) => void): number;
    connect_after(signal: "on-error", callback: (_source: this, error: GLib.Error) => void): number;
    emit(signal: "on-error", error: GLib.Error): void;
    connect(signal: "on-message-data", callback: (_source: this, data: GLib.Bytes | null) => void): number;
    connect_after(signal: "on-message-data", callback: (_source: this, data: GLib.Bytes | null) => void): number;
    emit(signal: "on-message-data", data: GLib.Bytes | null): void;
    connect(signal: "on-message-string", callback: (_source: this, data: string | null) => void): number;
    connect_after(signal: "on-message-string", callback: (_source: this, data: string | null) => void): number;
    emit(signal: "on-message-string", data: string | null): void;
    connect(signal: "on-open", callback: (_source: this) => void): number;
    connect_after(signal: "on-open", callback: (_source: this) => void): number;
    emit(signal: "on-open"): void;
    connect(signal: "send-data", callback: (_source: this, data: GLib.Bytes | null) => void): number;
    connect_after(signal: "send-data", callback: (_source: this, data: GLib.Bytes | null) => void): number;
    emit(signal: "send-data", data: GLib.Bytes | null): void;
    connect(signal: "send-string", callback: (_source: this, data: string | null) => void): number;
    connect_after(signal: "send-string", callback: (_source: this, data: string | null) => void): number;
    emit(signal: "send-string", data: string | null): void;

    // Members

    close(): void;
    on_buffered_amount_low(): void;
    on_close(): void;
    on_error(error: GLib.Error): void;
    on_message_data(data?: GLib.Bytes | null): void;
    on_message_string(str?: string | null): void;
    on_open(): void;
    send_data(data?: GLib.Bytes | null): void;
    send_string(str?: string | null): void;
    vfunc_close(): void;
    vfunc_send_data(data?: GLib.Bytes | null): void;
    vfunc_send_string(str?: string | null): void;
}
export module WebRTCICETransport {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
        component: WebRTCICEComponent;
        gathering_state: WebRTCICEGatheringState;
        gatheringState: WebRTCICEGatheringState;
        state: WebRTCICEConnectionState;
    }
}
export abstract class WebRTCICETransport extends Gst.Object {
    static $gtype: GObject.GType<WebRTCICETransport>;

    constructor(properties?: Partial<WebRTCICETransport.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCICETransport.ConstructorProperties>, ...args: any[]): void;

    // Properties
    component: WebRTCICEComponent;
    gathering_state: WebRTCICEGatheringState;
    gatheringState: WebRTCICEGatheringState;
    state: WebRTCICEConnectionState;

    // Fields
    role: WebRTCICERole;
    src: Gst.Element;
    sink: Gst.Element;

    // Signals

    connect(id: string, callback: (...args: any[]) => any): number;
    connect_after(id: string, callback: (...args: any[]) => any): number;
    emit(id: string, ...args: any[]): void;
    connect(signal: "on-new-candidate", callback: (_source: this, object: string) => void): number;
    connect_after(signal: "on-new-candidate", callback: (_source: this, object: string) => void): number;
    emit(signal: "on-new-candidate", object: string): void;
    connect(signal: "on-selected-candidate-pair-change", callback: (_source: this) => void): number;
    connect_after(signal: "on-selected-candidate-pair-change", callback: (_source: this) => void): number;
    emit(signal: "on-selected-candidate-pair-change"): void;

    // Members

    connection_state_change(new_state: WebRTCICEConnectionState): void;
    gathering_state_change(new_state: WebRTCICEGatheringState): void;
    new_candidate(stream_id: number, component: WebRTCICEComponent, attr: string): void;
    selected_pair_change(): void;
    vfunc_gather_candidates(): boolean;
}
export module WebRTCRTPReceiver {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
    }
}
export class WebRTCRTPReceiver extends Gst.Object {
    static $gtype: GObject.GType<WebRTCRTPReceiver>;

    constructor(properties?: Partial<WebRTCRTPReceiver.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCRTPReceiver.ConstructorProperties>, ...args: any[]): void;

    // Fields
    transport: WebRTCDTLSTransport;
    rtcp_transport: WebRTCDTLSTransport;

    // Constructors

    static ["new"](): WebRTCRTPReceiver;

    // Members

    set_rtcp_transport(transport: WebRTCDTLSTransport): void;
    set_transport(transport: WebRTCDTLSTransport): void;
}
export module WebRTCRTPSender {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
    }
}
export class WebRTCRTPSender extends Gst.Object {
    static $gtype: GObject.GType<WebRTCRTPSender>;

    constructor(properties?: Partial<WebRTCRTPSender.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCRTPSender.ConstructorProperties>, ...args: any[]): void;

    // Fields
    transport: WebRTCDTLSTransport;
    rtcp_transport: WebRTCDTLSTransport;
    send_encodings: any[];

    // Constructors

    static ["new"](): WebRTCRTPSender;

    // Members

    set_rtcp_transport(transport: WebRTCDTLSTransport): void;
    set_transport(transport: WebRTCDTLSTransport): void;
}
export module WebRTCRTPTransceiver {
    export interface ConstructorProperties extends Gst.Object.ConstructorProperties {
        [key: string]: any;
        direction: WebRTCRTPTransceiverDirection;
        mlineindex: number;
        receiver: WebRTCRTPReceiver;
        sender: WebRTCRTPSender;
    }
}
export abstract class WebRTCRTPTransceiver extends Gst.Object {
    static $gtype: GObject.GType<WebRTCRTPTransceiver>;

    constructor(properties?: Partial<WebRTCRTPTransceiver.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<WebRTCRTPTransceiver.ConstructorProperties>, ...args: any[]): void;

    // Properties
    direction: WebRTCRTPTransceiverDirection;
    mlineindex: number;
    receiver: WebRTCRTPReceiver;
    sender: WebRTCRTPSender;

    // Fields
    mline: number;
    mid: string;
    stopped: boolean;
    current_direction: WebRTCRTPTransceiverDirection;
    codec_preferences: Gst.Caps;
}

export class WebRTCSessionDescription {
    static $gtype: GObject.GType<WebRTCSessionDescription>;

    constructor(type: WebRTCSDPType, sdp: GstSdp.SDPMessage);
    constructor(copy: WebRTCSessionDescription);

    // Fields
    type: WebRTCSDPType;
    sdp: GstSdp.SDPMessage;

    // Constructors
    static ["new"](type: WebRTCSDPType, sdp: GstSdp.SDPMessage): WebRTCSessionDescription;

    // Members
    copy(): WebRTCSessionDescription;
    free(): void;
}
