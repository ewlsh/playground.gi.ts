/**
 * GstCodecs 1.0
 *
 * Generated from 1.0
 */

import * as Gst from "gst";
import * as GstVideo from "gstvideo";
import * as GObject from "gobject";
import * as GLib from "glib";

export const H264_DPB_MAX_SIZE: number;
export const H265_DPB_MAX_SIZE: number;

export namespace H264PictureField {
    export const $gtype: GObject.GType<H264PictureField>;
}

export enum H264PictureField {
    FRAME = 0,
    TOP_FIELD = 1,
    BOTTOM_FIELD = 2,
}

export namespace H265PictureField {
    export const $gtype: GObject.GType<H265PictureField>;
}

export enum H265PictureField {
    FIELD_FRAME = 0,
    FILED_TOP_FIELD = 1,
    FIELD_BOTTOM_FIELD = 2,
}
export module H264Decoder {
    export interface ConstructorProperties extends GstVideo.VideoDecoder.ConstructorProperties {
        [key: string]: any;
    }
}
export abstract class H264Decoder extends GstVideo.VideoDecoder {
    static $gtype: GObject.GType<H264Decoder>;

    constructor(properties?: Partial<H264Decoder.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<H264Decoder.ConstructorProperties>, ...args: any[]): void;

    // Members

    get_picture(system_frame_number: number): H264Picture;
    set_process_ref_pic_lists(process: boolean): void;
    vfunc_end_picture(picture: H264Picture): boolean;
    vfunc_new_picture(frame: GstVideo.VideoCodecFrame, picture: H264Picture): boolean;
    vfunc_output_picture(frame: GstVideo.VideoCodecFrame, picture: H264Picture): Gst.FlowReturn;
    vfunc_start_picture(picture: H264Picture, slice: H264Slice, dpb: H264Dpb): boolean;
}
export module H265Decoder {
    export interface ConstructorProperties extends GstVideo.VideoDecoder.ConstructorProperties {
        [key: string]: any;
    }
}
export abstract class H265Decoder extends GstVideo.VideoDecoder {
    static $gtype: GObject.GType<H265Decoder>;

    constructor(properties?: Partial<H265Decoder.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<H265Decoder.ConstructorProperties>, ...args: any[]): void;

    // Members

    vfunc_decode_slice(picture: H265Picture, slice: H265Slice): boolean;
    vfunc_end_picture(picture: H265Picture): boolean;
    vfunc_new_picture(picture: H265Picture): boolean;
    vfunc_output_picture(picture: H265Picture): Gst.FlowReturn;
    vfunc_start_picture(picture: H265Picture, slice: H265Slice, dpb: H265Dpb): boolean;
}
export module Vp8Decoder {
    export interface ConstructorProperties extends GstVideo.VideoDecoder.ConstructorProperties {
        [key: string]: any;
    }
}
export abstract class Vp8Decoder extends GstVideo.VideoDecoder {
    static $gtype: GObject.GType<Vp8Decoder>;

    constructor(properties?: Partial<Vp8Decoder.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<Vp8Decoder.ConstructorProperties>, ...args: any[]): void;

    // Members

    vfunc_end_picture(picture: Vp8Picture): boolean;
    vfunc_new_picture(frame: GstVideo.VideoCodecFrame, picture: Vp8Picture): boolean;
    vfunc_output_picture(frame: GstVideo.VideoCodecFrame, picture: Vp8Picture): Gst.FlowReturn;
    vfunc_start_picture(picture: Vp8Picture): boolean;
}
export module Vp9Decoder {
    export interface ConstructorProperties extends GstVideo.VideoDecoder.ConstructorProperties {
        [key: string]: any;
    }
}
export abstract class Vp9Decoder extends GstVideo.VideoDecoder {
    static $gtype: GObject.GType<Vp9Decoder>;

    constructor(properties?: Partial<Vp9Decoder.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<Vp9Decoder.ConstructorProperties>, ...args: any[]): void;

    // Members

    vfunc_decode_picture(picture: Vp9Picture, dpb: Vp9Dpb): boolean;
    vfunc_duplicate_picture(picture: Vp9Picture): Vp9Picture;
    vfunc_end_picture(picture: Vp9Picture): boolean;
    vfunc_new_picture(frame: GstVideo.VideoCodecFrame, picture: Vp9Picture): boolean;
    vfunc_output_picture(frame: GstVideo.VideoCodecFrame, picture: Vp9Picture): Gst.FlowReturn;
    vfunc_start_picture(picture: Vp9Picture): boolean;
}

export class H264DecoderPrivate {
    static $gtype: GObject.GType<H264DecoderPrivate>;

    constructor(copy: H264DecoderPrivate);
}

export class H264Dpb {
    static $gtype: GObject.GType<H264Dpb>;

    constructor(copy: H264Dpb);

    // Members
    add(picture: H264Picture): void;
    clear(): void;
    delete_by_poc(poc: number): void;
    delete_outputed(): void;
    delete_unused(): void;
    free(): void;
    get_long_ref_by_pic_num(pic_num: number): H264Picture | null;
    get_lowest_frame_num_short_ref(): H264Picture;
    get_max_num_pics(): number;
    get_picture(system_frame_number: number): H264Picture;
    get_pictures_all(): H264Picture[];
    get_pictures_long_term_ref(): H264Picture[];
    get_pictures_not_outputted(): H264Picture[];
    get_pictures_short_term_ref(): H264Picture[];
    get_short_ref_by_pic_num(pic_num: number): H264Picture | null;
    get_size(): number;
    is_full(): boolean;
    mark_all_non_ref(): void;
    num_ref_pictures(): number;
    set_max_num_pics(max_num_pics: number): void;
}

export class H264Picture {
    static $gtype: GObject.GType<H264Picture>;

    constructor();
    constructor(copy: H264Picture);

    // Fields
    pts: Gst.ClockTime;
    system_frame_number: number;
    pic_order_cnt_type: number;
    top_field_order_cnt: number;
    bottom_field_order_cnt: number;
    pic_order_cnt: number;
    pic_order_cnt_msb: number;
    pic_order_cnt_lsb: number;
    delta_pic_order_cnt_bottom: number;
    delta_pic_order_cnt0: number;
    delta_pic_order_cnt1: number;
    pic_num: number;
    long_term_pic_num: number;
    frame_num: number;
    frame_num_offset: number;
    frame_num_wrap: number;
    long_term_frame_idx: number;
    nal_ref_idc: number;
    idr: boolean;
    idr_pic_id: number;
    ref: boolean;
    long_term: boolean;
    outputted: boolean;
    mem_mgmt_5: boolean;
    nonexisting: boolean;
    field: H264PictureField;
    user_data: any;
    notify: GLib.DestroyNotify;

    // Constructors
    static ["new"](): H264Picture;

    // Members
    get_user_data(): any | null;
    set_user_data(notify: GLib.DestroyNotify): void;
}

export class H264Slice {
    static $gtype: GObject.GType<H264Slice>;

    constructor(copy: H264Slice);
}

export class H265DecoderPrivate {
    static $gtype: GObject.GType<H265DecoderPrivate>;

    constructor(copy: H265DecoderPrivate);
}

export class H265Dpb {
    static $gtype: GObject.GType<H265Dpb>;

    constructor(copy: H265Dpb);

    // Members
    add(picture: H265Picture): void;
    clear(): void;
    delete_by_poc(poc: number): void;
    delete_unused(): void;
    free(): void;
    get_long_ref_by_poc(poc: number): H265Picture | null;
    get_max_num_pics(): number;
    get_pictures_all(): H265Picture[];
    get_pictures_not_outputted(): H265Picture[];
    get_ref_by_poc(poc: number): H265Picture | null;
    get_ref_by_poc_lsb(poc_lsb: number): H265Picture | null;
    get_short_ref_by_poc(poc: number): H265Picture | null;
    get_size(): number;
    is_full(): boolean;
    mark_all_non_ref(): void;
    num_ref_pictures(): number;
    set_max_num_pics(max_num_pics: number): void;
}

export class H265Picture {
    static $gtype: GObject.GType<H265Picture>;

    constructor();
    constructor(copy: H265Picture);

    // Fields
    pts: Gst.ClockTime;
    system_frame_number: number;
    pic_order_cnt: number;
    pic_order_cnt_msb: number;
    pic_order_cnt_lsb: number;
    pic_latency_cnt: number;
    output_flag: boolean;
    NoRaslOutputFlag: boolean;
    NoOutputOfPriorPicsFlag: boolean;
    RapPicFlag: boolean;
    IntraPicFlag: boolean;
    ref: boolean;
    long_term: boolean;
    outputted: boolean;
    field: H265PictureField;
    user_data: any;
    notify: GLib.DestroyNotify;

    // Constructors
    static ["new"](): H265Picture;

    // Members
    get_user_data(): any | null;
    set_user_data(notify: GLib.DestroyNotify): void;
}

export class H265Slice {
    static $gtype: GObject.GType<H265Slice>;

    constructor(copy: H265Slice);
}

export class Vp8DecoderPrivate {
    static $gtype: GObject.GType<Vp8DecoderPrivate>;

    constructor(copy: Vp8DecoderPrivate);
}

export class Vp8Picture {
    static $gtype: GObject.GType<Vp8Picture>;

    constructor();
    constructor(copy: Vp8Picture);

    // Fields
    pts: Gst.ClockTime;
    system_frame_number: number;
    data: number;
    size: number;
    user_data: any;
    notify: GLib.DestroyNotify;

    // Constructors
    static ["new"](): Vp8Picture;

    // Members
    get_user_data(): any | null;
    set_user_data(notify: GLib.DestroyNotify): void;
}

export class Vp9DecoderPrivate {
    static $gtype: GObject.GType<Vp9DecoderPrivate>;

    constructor(copy: Vp9DecoderPrivate);
}

export class Vp9Dpb {
    static $gtype: GObject.GType<Vp9Dpb>;

    constructor(copy: Vp9Dpb);

    // Fields
    pic_list: Vp9Picture[];

    // Members
    add(picture: Vp9Picture): void;
    clear(): void;
    free(): void;
}

export class Vp9Picture {
    static $gtype: GObject.GType<Vp9Picture>;

    constructor();
    constructor(copy: Vp9Picture);

    // Fields
    pts: Gst.ClockTime;
    system_frame_number: number;
    subsampling_x: number;
    subsampling_y: number;
    bit_depth: number;
    data: number;
    size: number;
    user_data: any;
    notify: GLib.DestroyNotify;

    // Constructors
    static ["new"](): Vp9Picture;

    // Members
    get_user_data(): any | null;
    set_user_data(notify: GLib.DestroyNotify): void;
}
