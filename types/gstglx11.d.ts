/**
 * GstGLX11 1.0
 *
 * Generated from 1.0
 */

import * as Gst from "gst";
import * as GstBase from "gstbase";
import * as GstGL from "gstgl";
import * as GstVideo from "gstvideo";
import * as GObject from "gobject";

export module GLDisplayX11 {
    export interface ConstructorProperties extends GstGL.GLDisplay.ConstructorProperties {
        [key: string]: any;
    }
}
export class GLDisplayX11 extends GstGL.GLDisplay {
    static $gtype: GObject.GType<GLDisplayX11>;

    constructor(properties?: Partial<GLDisplayX11.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<GLDisplayX11.ConstructorProperties>, ...args: any[]): void;

    // Constructors

    static ["new"](name?: string | null): GLDisplayX11;
    static ["new"](...args: never[]): never;
}
