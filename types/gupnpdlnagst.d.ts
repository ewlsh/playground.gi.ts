/**
 * GUPnPDLNAGst 2.0
 *
 * Generated from 2.0
 */

import * as GObject from "gobject";
import * as GUPnPDLNA from "gupnpdlna";
import * as Gst from "gst";
import * as GstPbutils from "gstpbutils";

export function utils_information_from_discoverer_info(info: GstPbutils.DiscovererInfo): GUPnPDLNA.Information;
