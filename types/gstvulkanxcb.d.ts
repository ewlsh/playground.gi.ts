/**
 * GstVulkanXCB 1.0
 *
 * Generated from 1.0
 */

import * as Gst from "gst";
import * as GstBase from "gstbase";
import * as GstVideo from "gstvideo";
import * as GstVulkan from "gstvulkan";
import * as Vulkan from "vulkan";
import * as GObject from "gobject";

export module VulkanDisplayXCB {
    export interface ConstructorProperties extends GstVulkan.VulkanDisplay.ConstructorProperties {
        [key: string]: any;
    }
}
export class VulkanDisplayXCB extends GstVulkan.VulkanDisplay {
    static $gtype: GObject.GType<VulkanDisplayXCB>;

    constructor(properties?: Partial<VulkanDisplayXCB.ConstructorProperties>, ...args: any[]);
    _init(properties?: Partial<VulkanDisplayXCB.ConstructorProperties>, ...args: any[]): void;

    // Constructors

    static ["new"](name?: string | null): VulkanDisplayXCB;
    static ["new"](...args: never[]): never;
}
